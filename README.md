# MOVED
Now being updated in https://gitlab.com/dawsoncollege/infra-linux2-440/-/tree/master/sockets

this repo is deprecated

# code for creating sockets
For teaching/learning about sockets, playiing with the underlying TCP transport layer communications.  TCP is used for a lot of application layer protocols.

Note it would be interesting to peek at the communications as they happen using https://www.wireshark.org/
#  echo
see [server.py](server.py) when you run the server the process logs to socket.log (appends, you may have to delete old)
## server side
Run the server code, it will bind to tcp (stream) & ip & port
* run in background `./server.py &`
   * binds to 127.0.0.1:8111
   * check it with `netstat -lan |less`  note the state LISTEN
   * if it crashes or has just ended the ip&port may still be bound, in a TIME_WAIT status instead of LISTEN,
     you can see this through netstat, you will have to wa,it for it to end
   * you can see the comms via socket.log in the cwd
## client side, talk to server
On the same system, use netcat (nc) to act as a client & make the socket pair
* fake out client comms with netcat 
   * talk to the server `nc -v -n 127.0.0.1 8111`
   * it will echo the data you type in back at you, as long as the socket pair is live
   * `quit` will end the connection  on the server side
   * you will have to hit enter again after entering quit as that will signal nc to stop
   * check the socket source & dest while you are talking to the server `netstat -lan|less` and after you quit (you may see TIME_WAIT)
## Use the code in a lab exercise 
Follow the instructions in [lab-questions](lab-questions.md) to play with and learn about TCP sockets.
